#!/bin/bash

#CAMOMILE_BIN="/home/pi/src/Camomile/Plugins/camomile"
SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
CAMOMILE_BIN="$SCRIPTDIR/lib/Camomile/Plugins/camomile"
#sqCAMOMILE_BIN="/Users/gllm/Documents/_sync/barista/home/sketchs/pd/camomile-builder/lib/camo-git/Camomile/Plugins/camomile"

CAMOMILE_PATH=$(dirname "$CAMOMILE_BIN")

SRCDIR="$SCRIPTDIR/src"
BUILDDIR="$SCRIPTDIR/builds/$(uname)_$(uname -m)"

mkdir -p $BUILDDIR

"$CAMOMILE_BIN" -d "$SRCDIR" -o "$BUILDDIR"

