mkdir -p ~/.config/systemd/user/

SERVICE=reaper
echo "Deploying $SERVICE"
cp $SERVICE.service  ~/.config/systemd/user/

systemctl --user daemon-reload
systemctl --user disable $SERVICE.service

SERVICE=dm-pd
echo "Deploying $SERVICE"
cp $SERVICE.service  ~/.config/systemd/user/

systemctl --user daemon-reload
systemctl --user disable $SERVICE.service

SERVICE=dm-sampler
echo "Deploying $SERVICE"
cp $SERVICE.service  ~/.config/systemd/user/

systemctl --user daemon-reload
systemctl --user disable $SERVICE.service

SERVICE=dm-qjackctl
echo "Deploying $SERVICE"
cp $SERVICE.service  ~/.config/systemd/user/

systemctl --user daemon-reload
systemctl --user disable $SERVICE.service

SERVICE=dm-init
echo "Deploying $SERVICE"
cp $SERVICE.service  ~/.config/systemd/user/

systemctl --user daemon-reload
systemctl --user disable $SERVICE.service
systemctl --user enable $SERVICE.service

