# dm-dawplugin

[Distributed memories](https://gitlab.com/gllmar/distributed-memories) Daw plugin usign camomile

## Install

### dependencies

```
sudo apt-get install cmake libasound2-dev  libfreetype-dev libcurl4 libwebkit2gtk-4.0-dev
```

### clone this repo with submodules
```
git clone --recursive  git@gitlab.com:gllmar/dm-dawplugin.git

```
### build camomile

```
cd dm-dawplugin/lib/Camomile
mkdir build
cd build
cmake ..
make -j4
```


## Composantes

### dm_dispatcher

#### video
* quand message Alive, renvoyer un nouveau video

#### Bouton
* Quand 1 ou 0 nouveaux à tous les samplers
* Lance un son après avoir reset
* Parle à touch designer
* multicast a `239.200.200.200:50555`

#### Touch Designer


#### Detection de présence
* kinect 1 port 54321
* kinect 2 port 54322
* kinect 3 port 54323
* kinect 4 port 54324
* kinect 5 port 54325

###


## Protocole
```mermaid
graph LR


subgraph td_tdvideo
end

subgraph dm_rpvideo
end

subgraph dm_sampler
dm_sampler_fudi_port[55010++6]
end

subgraph dm_tracker
dm_tracker_fudi_port[55110++6]
end

subgraph dm_dispatch
end
 

```


